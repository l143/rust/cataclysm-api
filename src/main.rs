use cataclysm::{Server, Branch};
use std::sync::{Mutex, Arc};

use cataclysmapi::modules::{
    api, messages, storage::infra::Repository,
    messages::domain::message::Message,
    messages::infra::storage::local::LocalRepository
};


#[tokio::main]
async fn main() {
    let stack: Vec<Message> = [].to_vec();
    let messages = Arc::new(Mutex::new(stack));

    let repo = Repository{
        messages: LocalRepository::new(messages)
    };

    let api_branch = api::infra::get_endpoints();
    let message_branch = messages::infra::api::get_endpoints();

    let main_branch = Branch::new("/api")
        .nest(
            api_branch
            .merge(message_branch)
        );

    let server = Server::builder(main_branch)
        .share(repo.into())
        .build().unwrap();
    // And we launch it on the following address
    server.run("127.0.0.1:8000").await.unwrap();
}