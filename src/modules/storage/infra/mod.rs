use crate::modules::messages;

// pub struct Repository<T: MessageRepository> {
// 	pub messages: T
// }



pub struct Repository {
    pub messages: messages::infra::storage::local::LocalRepository
}