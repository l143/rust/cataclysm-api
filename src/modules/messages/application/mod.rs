use crate::modules::messages::domain::{repository::MessageRepository, message::Message};
use crate::modules::messages::dtos::CreateMessage;

pub struct MessageService<T: MessageRepository> {
  pub repo: T
}

impl<T: MessageRepository> MessageService<T> {
  pub fn create(&mut self, m: CreateMessage) -> Result<Message, String> {
    return self.repo.save(m)
  }
}

