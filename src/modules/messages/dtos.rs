use serde::{Deserialize};

#[derive(Deserialize, Clone)]
pub struct CreateMessage {
	pub title: String,
	pub owner: u64
}

pub struct UpdateMessage {
	pub title: Option<String>,
	pub owner: Option<u64>,
}