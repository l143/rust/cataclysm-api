use std::sync::{Arc, Mutex};

use crate::modules::messages::dtos::CreateMessage;
use crate::modules::messages::domain::{repository::MessageRepository, message::Message};

#[derive(Clone)]
pub struct LocalRepository {
	stack: Arc<Mutex<Vec<Message>>>
}


impl MessageRepository for LocalRepository {
	fn save(&mut self, m: CreateMessage) -> Result<Message, String> {
		let mut tmp = self.stack.lock().unwrap();

		let count = tmp.len() as u64;
		let new_message = Message {
			id: count + 1,
			title: m.title,
			owner: 1
		};
		tmp.push(new_message.clone());
		println!("{:?}", tmp);
		return Ok(new_message);
	}

}

impl LocalRepository {
	pub fn new(stack: Arc<Mutex<Vec<Message>>>) -> LocalRepository {
		return LocalRepository{stack: stack};
	}
}