use cataclysm::{Branch, Shared, http::{Response, Method}};
use crate::modules::storage::infra::Repository;
use crate::modules::messages::{
	dtos::CreateMessage,
	application::MessageService
};

use serde_json;


async fn create(payload: String, shared: Shared<Repository>) -> Response {
	let repo = shared.into_inner();
	let mut svc =  MessageService{repo: repo.messages.clone()};
	let m: CreateMessage = match serde_json::from_str(&payload) {
		Ok(m) => m,
		Err(e) => {
			let err_msg = format!("Incorrect payload: {}", e);
			return Response::bad_request().body(err_msg);
		}
	};
	match svc.create(m.clone()) {
		Ok(m) => {
			let res = match serde_json::to_string(&m) {
				Ok(r) => r,
				Err(_) => String::from("")
			};
			Response::ok().body(res)
		},
		Err(e) => {
			Response::internal_server_error().body(e)
		}
	}
}

pub fn get_endpoints() -> Branch<Repository> {
	return Branch::new("/message")
		.with(Method::Post.to(create))
}