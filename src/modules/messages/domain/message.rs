use serde::{Serialize};

#[derive(Clone, Serialize, Debug)]
pub struct Message {
	pub id: u64,
	pub title: String,
	pub owner: u64
}