use crate::modules::messages::{domain::message::Message, dtos::CreateMessage};

pub trait MessageRepository {
	fn save(&mut self, m: CreateMessage) -> Result<Message, String>;
}