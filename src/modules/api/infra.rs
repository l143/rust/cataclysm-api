use crate::modules::storage::infra::Repository;
use cataclysm::{Branch, http::{Response, Method}};

async fn version() -> Response {
	Response::ok().body("v1")
}


pub fn get_endpoints() -> Branch<Repository> {
	return Branch::new("/version")
		.with(Method::Get.to(version))
}