# Cataclysm Api

Example of cataclysm api exposure.

It tries to use SOLID principles to have a better organization and implementation code.

- **Infra**  
Infrastructure layer. Everything related to the entrypoint of the program, for example de api endpoints or the cli commands.

- **Application**  
Here are the services will help us to manage the entities in our program like users, messages, roles, files, etc... It should not have framework, database, notification or other dependencies of the same type so we can change them easily based in Traits.

- **Domain**  
This scope has the internal structures for mapping with the db. Idealy as in the Application layer, it should not have external dependencies but it also should define de Traits for persistant managent or similar.

- **Dtos**  
(Data transfer objects) As it's name suggest, these stucts will help us receive data and pass it to our services or stortage implementations.

## Try-it

```bash
$ cargo run
```

> See api version 

```bash
curl http://localhost:8000/api/version
```

> Register messages

```bash
curl -X POST http://localhost:8000/api/message -d '{"title": "some", "owner": 1}'
```

